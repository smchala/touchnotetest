###TouchNote test####

2 views, the first: recyclerview with toggle between LinearLayout and GridLayour, once an item is selected it opens the second view which can be cropped sliding a control.
Work in progress: Preserve state when device rotate...
example of unit and functional testing is present.
Architecture: MVP, ideally if I had more time I would have used the 3 tiers architecture (CLEAN) Presentation, Domain and Data. the current setup can easily be modified.
Please note: neither errors nor progress have been implemented, however you can find placeholders!

###Libraries:###
* rxandroid
* retrofit
* picasso
* gson
* junit
* espresso
* started implementing dagger2...