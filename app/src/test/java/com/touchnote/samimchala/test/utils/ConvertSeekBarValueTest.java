package com.touchnote.samimchala.test.utils;

import com.touchnote.samimchala.test.views.utils.ConvertSeekBarValue;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConvertSeekBarValueTest {
    @Test
    public void convertPositiveIntToFloat() throws Exception {
        float actual = ConvertSeekBarValue.convertSeekBarValueToFloat(2);
        float expected = 0.02f;
        assertEquals(actual, expected, 0);
    }

    @Test
    public void conversionShouldOnlyBeForPositiveValues() throws Exception {
        float actual = ConvertSeekBarValue.convertSeekBarValueToFloat(-2);
        float expected = 0.0f;
        assertEquals(actual, expected, 0);
    }

    @Test
    public void conversionShouldOnlyBeForValuesNoGreaterThan100() throws Exception {
        float actual = ConvertSeekBarValue.convertSeekBarValueToFloat(101);
        float expected = 0.0f;
        assertEquals(actual, expected, 0);
    }

    @Test
    public void getCroppedImageValue() throws Exception {
        int actual = ConvertSeekBarValue.getCroppedImageValue(1, 1.0f);
        int expected = 1;
        assertEquals(actual, expected);
    }

    @Test
    public void getCroppedImagePositiveValueOnly() throws Exception {
        int actual = ConvertSeekBarValue.getCroppedImageValue(-1, 1.0f);
        int expected = 0;
        assertEquals(actual, expected);
    }

}
