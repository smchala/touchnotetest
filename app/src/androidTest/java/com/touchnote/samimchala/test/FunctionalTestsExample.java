package com.touchnote.samimchala.test;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.touchnote.samimchala.test.views.ImagesActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.touchnote.samimchala.test.FunctionalTestHelpers.matchToolbarTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class FunctionalTestsExample {

    @Before
    public void setUp(){}

    @Test
    public void useAppContextTest() throws Exception {
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.touchnote.samimchala.test", appContext.getPackageName());
    }

    @Test
    public void toolbarTitleTest() {
        CharSequence title = InstrumentationRegistry
                .getTargetContext().getString(R.string.app_name);
        matchToolbarTitle(title);
    }

    @Rule
    public ActivityTestRule<ImagesActivity> mActivityRule = new ActivityTestRule(ImagesActivity.class);

    @Test
    public void checkNumberOfItemsInRecyclerViewTest(){
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(10));
    }

    @Test
    public void checkRecyclerViewFirstItemTest(){
        onView(withRecyclerView(R.id.recyclerView)
                .atPositionOnView(0, R.id.title))
                .check(matches(withText("enim quasi non incidunt veritatis magni rerum autem voluptatibus perferendis ratione")));
    }

    @After
    public void tearDown(){}

    private static FunctionalTestHelpers withRecyclerView(final int recyclerViewId) {
        return new FunctionalTestHelpers(recyclerViewId);
    }

    public class RecyclerViewItemCountAssertion implements ViewAssertion {
        private final int expectedCount;

        public RecyclerViewItemCountAssertion(int expectedCount) {
            this.expectedCount = expectedCount;
        }

        @Override
        public void check(View view, NoMatchingViewException noViewFoundException) {
            if (noViewFoundException != null) {
                throw noViewFoundException;
            }

            RecyclerView recyclerView = (RecyclerView) view;
            RecyclerView.Adapter adapter = recyclerView.getAdapter();
            assertThat(adapter.getItemCount(), is(expectedCount));
        }
    }
}
