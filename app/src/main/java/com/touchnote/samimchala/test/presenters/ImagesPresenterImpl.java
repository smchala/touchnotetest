package com.touchnote.samimchala.test.presenters;

import android.os.Bundle;
import android.util.Log;

import com.touchnote.samimchala.test.models.Image;
import com.touchnote.samimchala.test.models.cloud.ImagesService;
import com.touchnote.samimchala.test.views.ImagesView;

import java.util.ArrayList;

import retrofit2.Response;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ImagesPresenterImpl implements ImagesPresenter {

    private ImagesService imagesService;
    private Observable<Response<ArrayList<Image>>> imagesObservable;
    private Subscription subscription;
    private ImagesView imagesView;
    private boolean menuIconState;
    private Bundle savedInstanceState;

    public ImagesPresenterImpl(ImagesView imagesView, ImagesService imagesService) {
        this.imagesView = imagesView;
        this.imagesService = imagesService;
    }

    public void getImages(){
        imagesObservable = imagesService.getImagesAPI().getImagesObservable();
        subscription = imagesObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Response<ArrayList<Image>>>() {
            @Override
            public void onCompleted() {}

            @Override
            public void onError(Throwable e) {
                Log.e("sm", "ERROR: "+e.getMessage());
            }

            @Override
            public void onNext(Response<ArrayList<Image>> images) {
                imagesView.setImages(images.body());
            }
        });
    }

    @Override
    public void onCreated(Bundle savedInstanceState) {
        if(savedInstanceState!=null){
            this.savedInstanceState = savedInstanceState;
        }
    }

    @Override
    public void onResumed() {
        getImages();
    }

    @Override
    public void onDestroyed() {
        if(subscription!=null && !subscription.isUnsubscribed())
            subscription.unsubscribe();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(outState !=null && savedInstanceState !=null)
        savedInstanceState.putAll(outState);
    }

    @Override
    public void onImageSelected(int position) {
        imagesView.launchSelectedImageActivity(position);
    }

    @Override
    public boolean onToggleImagesView() {
        menuIconState = !menuIconState;
        return menuIconState;
    }
}
