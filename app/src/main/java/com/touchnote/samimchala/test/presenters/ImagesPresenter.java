package com.touchnote.samimchala.test.presenters;

import android.os.Bundle;

public interface ImagesPresenter {
    void onCreated(Bundle savedInstanceState);
    void onResumed();
    void onDestroyed();
    void onSaveInstanceState(Bundle outState);
    void onImageSelected(int position);
    boolean onToggleImagesView();
}
