package com.touchnote.samimchala.test.presenters;

public interface ImageCircleCroppingPresenter {
    void onSeekBarValueChanged(int value, int width);
}