package com.touchnote.samimchala.test.models.cloud;

public class Configuration {
    public static final String BASE_URL = "http://www.mocky.io";
    public static final String END_POINT = "/v2/57ee2ca8260000f80e1110fa";
    public static final String IMAGE_URL = "IMAGE_URL";
}
