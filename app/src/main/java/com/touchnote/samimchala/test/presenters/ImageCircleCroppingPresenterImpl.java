package com.touchnote.samimchala.test.presenters;

import com.touchnote.samimchala.test.views.ImageCircleCroppingView;
import com.touchnote.samimchala.test.views.utils.ConvertSeekBarValue;

public class ImageCircleCroppingPresenterImpl implements ImageCircleCroppingPresenter {
    private ImageCircleCroppingView imageCircleCroppingView;
    private  float seekBarValue;
    private int croppedImageValue;

    public ImageCircleCroppingPresenterImpl(ImageCircleCroppingView imageCircleCroppingView) {
        this.imageCircleCroppingView = imageCircleCroppingView;
    }

    @Override
    public void onSeekBarValueChanged(int value, int width) {
        seekBarValue = ConvertSeekBarValue.convertSeekBarValueToFloat(value);
        croppedImageValue = ConvertSeekBarValue.getCroppedImageValue(width, seekBarValue);
        imageCircleCroppingView.setImageCropValue(croppedImageValue);
    }
}
