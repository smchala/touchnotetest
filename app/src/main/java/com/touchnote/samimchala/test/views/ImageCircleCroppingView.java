package com.touchnote.samimchala.test.views;

public interface ImageCircleCroppingView {
    void setImageCropValue(int value);
    void showImagesLoadingProgress();
    void hideImagesLoadingProgress();
    void showError(String error);
}
