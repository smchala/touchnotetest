package com.touchnote.samimchala.test.views.utils;

import android.content.Context;

import com.touchnote.samimchala.test.presenters.ImageCircleCroppingPresenter;
import com.touchnote.samimchala.test.presenters.ImageCircleCroppingPresenterImpl;
import com.touchnote.samimchala.test.views.ImageCircleCroppingActivity;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    public ApplicationModule() {
    }

    @Provides
    public ImageCircleCroppingPresenter provideImagesPresenter(ImageCircleCroppingActivity imagesView){
        return new ImageCircleCroppingPresenterImpl(imagesView);
    }
}
