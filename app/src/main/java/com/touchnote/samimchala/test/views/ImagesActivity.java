package com.touchnote.samimchala.test.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.touchnote.samimchala.test.R;
import com.touchnote.samimchala.test.models.Image;
import com.touchnote.samimchala.test.models.cloud.Configuration;
import com.touchnote.samimchala.test.models.cloud.ImagesService;
import com.touchnote.samimchala.test.presenters.ImagesPresenter;
import com.touchnote.samimchala.test.presenters.ImagesPresenterImpl;
import com.touchnote.samimchala.test.views.RecyclerView.ImagesRecyclerViewGridAdapter;
import com.touchnote.samimchala.test.views.RecyclerView.ImagesRecyclerViewListAdapter;

import java.util.ArrayList;

public class ImagesActivity extends AppCompatActivity implements ImagesView{
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ImagesPresenter imagesPresenter;
    private ImagesService imagesService;
    private boolean state;
    private ArrayList<Image> images;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.images_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        imagesService = new ImagesService();
        imagesPresenter = new ImagesPresenterImpl(this, imagesService);
        imagesPresenter.onCreated(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        imagesPresenter.onResumed();
    }

    @Override
    protected void onDestroy() {
        imagesPresenter.onDestroyed();
        super.onDestroy();
    }

    @Override
    public void launchSelectedImageActivity(int position) {
        Intent intent = new Intent(this, ImageCircleCroppingActivity.class);
        intent.putExtra(Configuration.IMAGE_URL,images.get(position).getImage());
        startActivity(intent);
    }

    @Override
    public void setImages(ArrayList<Image> images) {
        this.images = images;
        setLayoutManager();
    }

    @Override
    public void showImagesLoadingProgress() {
    //    TODO:
    }

    @Override
    public void hideImagesLoadingProgress() {
    //    TODO:
    }

    @Override
    public void showError(String error) {
    //    TODO:
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        imagesPresenter.onSaveInstanceState(outState);
        //TODO: save/restore layoutManager into/from  savedInstanceState/onRestore
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_images:
                state = imagesPresenter.onToggleImagesView();
                setIcon(item, state);
                setLayoutManager();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setLayoutManager() {
        if (state) {
            setGridLayoutManager();
        }else {
            setLinearLayoutManager();
        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void setGridLayoutManager() {
        layoutManager = new GridLayoutManager(this, 2);
        adapter = new ImagesRecyclerViewGridAdapter(this, images);
        ((ImagesRecyclerViewGridAdapter)adapter).setOnItemClickListener(new ImagesRecyclerViewGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                imagesPresenter.onImageSelected(position);
            }
        });
    }
    private void setLinearLayoutManager() {
        layoutManager = new LinearLayoutManager(this);
        adapter = new ImagesRecyclerViewListAdapter(this, images);
        ((ImagesRecyclerViewListAdapter)adapter).setOnItemClickListener(new ImagesRecyclerViewListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                imagesPresenter.onImageSelected(position);
            }
        });
    }

    private void setIcon(MenuItem item, boolean state) {
        item.setIcon(state ? getResources().getDrawable(R.drawable.grid_icon) : getResources().getDrawable(R.drawable.list_icon));
    }
}
