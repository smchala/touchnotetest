package com.touchnote.samimchala.test.models.cloud;

import com.touchnote.samimchala.test.models.Image;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import rx.Observable;

public interface ImagesAPI {
    @Headers({
            "Content-Type: application/json"
    })
    @GET(Configuration.END_POINT)
    Observable<Response<ArrayList<Image>>> getImagesObservable();
}
