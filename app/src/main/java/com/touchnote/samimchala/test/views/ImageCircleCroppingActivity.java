package com.touchnote.samimchala.test.views;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.touchnote.samimchala.test.R;
import com.touchnote.samimchala.test.models.cloud.Configuration;
import com.touchnote.samimchala.test.presenters.ImageCircleCroppingPresenter;
//import com.touchnote.samimchala.test.views.utils.ApplicationComponent;
//import com.touchnote.samimchala.test.views.utils.ApplicationModule;
//import com.touchnote.samimchala.test.views.utils.DaggerApplicationComponent;
import com.touchnote.samimchala.test.presenters.ImageCircleCroppingPresenterImpl;
import com.touchnote.samimchala.test.views.utils.WindowSize;

import javax.inject.Inject;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class ImageCircleCroppingActivity extends AppCompatActivity implements ImageCircleCroppingView{

    private ImageCircleCroppingPresenter imageCircleCroppingPresenter;
    private SeekBar seekBar;
    private ImageView imageView;
    private int width;
    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            imageView.setImageBitmap(bitmap);
            Log.d("sm","from.name(): "+from.name());
        }
        @Override
        public void onBitmapFailed(Drawable errorDrawable) {}
        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {}
    };
    private String imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        ApplicationComponent applicationComponent = DaggerApplicationComponent.builder()
//                .applicationModule(new ApplicationModule())
//                .build();
//        applicationComponent.inject(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_crop_view);

        if(getIntent().getExtras() !=null) {
            imageUrl = getIntent().getStringExtra(Configuration.IMAGE_URL);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        width = WindowSize.getWidth(this);
        seekBar = (SeekBar)findViewById(R.id.seekBar);
        imageView = (ImageView)findViewById(R.id.image1);

        imageCircleCroppingPresenter = new ImageCircleCroppingPresenterImpl(this);

        Picasso.with(this)
                .load(imageUrl)
                .resize(width, width)
                .centerCrop()
                .error(R.drawable.errorstop)
                .into(target);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                imageCircleCroppingPresenter.onSeekBarValueChanged(seekBar.getProgress(), width);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void circleCrop(int value) {
        Picasso.with(ImageCircleCroppingActivity.this)
                .load(imageUrl)
                .transform(new RoundedCornersTransformation(value, value))
                .resize(width,width)
                .centerCrop()
                .error(R.drawable.errorstop)
                .into(target);
    }

    @Override
    public void setImageCropValue(int value) {
            circleCrop(value);
    }

    @Override
    public void showImagesLoadingProgress() {
//        TODO:...
    }

    @Override
    public void hideImagesLoadingProgress() {
//        TODO:...
    }

    @Override
    public void showError(String error) {
//        TODO:...
    }
}
