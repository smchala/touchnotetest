package com.touchnote.samimchala.test.views.utils;

public class ConvertSeekBarValue {
    public static float convertSeekBarValueToFloat(int value) {
        if(value >=0 && value <=100)
        return value/100f;
        return 0.0f;
    }

    public static int getCroppedImageValue(int width, float value){
        if(width>=0 && value >= 0)
        return (int)(width*value);
        return 0;
    }
}
