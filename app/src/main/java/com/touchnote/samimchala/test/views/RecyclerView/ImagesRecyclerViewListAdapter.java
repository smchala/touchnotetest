package com.touchnote.samimchala.test.views.RecyclerView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.touchnote.samimchala.test.R;
import com.touchnote.samimchala.test.models.Image;

import java.util.ArrayList;

public class ImagesRecyclerViewListAdapter extends RecyclerView.Adapter<ImagesRecyclerViewListAdapter.ImagesListViewHolder> {

    private final ArrayList<Image> images;
    private Context context;
    private static OnItemClickListener listener;

    public ImagesRecyclerViewListAdapter(Context context, ArrayList<Image> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public ImagesListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.
                from(context).inflate(R.layout.list_item_layout, parent, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        ImagesListViewHolder imagesListViewHolder = new ImagesListViewHolder(rootView);
        return imagesListViewHolder;
    }

    @Override
    public void onBindViewHolder(ImagesListViewHolder holder, int position) {

        holder.titleTv.setText(images.get(position).getTitle());
        holder.descTv.setText(images.get(position).getDescription());
        Picasso.with(context)
                .load(images.get(position).getImage())
                .error(R.drawable.errorstop)
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        if (images != null) {
            return images.size();
        }
        return 0;
    }

    @Override
    public void onViewRecycled(ImagesListViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public static class ImagesListViewHolder extends RecyclerView.ViewHolder{
        TextView titleTv;
        TextView descTv;
        ImageView imageView;

        ImagesListViewHolder(final View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            descTv = (TextView) itemView.findViewById(R.id.description);
            titleTv = (TextView) itemView.findViewById(R.id.title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }
}
