package com.touchnote.samimchala.test.views.utils;

import com.touchnote.samimchala.test.views.ImageCircleCroppingActivity;

import dagger.Component;

@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(ImageCircleCroppingActivity imageCircleCroppingActivity);
}
