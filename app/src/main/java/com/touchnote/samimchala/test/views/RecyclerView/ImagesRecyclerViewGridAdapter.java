package com.touchnote.samimchala.test.views.RecyclerView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.touchnote.samimchala.test.R;
import com.touchnote.samimchala.test.models.Image;

import java.util.ArrayList;

public class ImagesRecyclerViewGridAdapter extends RecyclerView.Adapter<ImagesRecyclerViewGridAdapter.ImagesGridViewHolder> {


    private final ArrayList<Image> images;
    private Context context;
    private static OnItemClickListener listener;

    public ImagesRecyclerViewGridAdapter(Context context, ArrayList<Image> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public ImagesRecyclerViewGridAdapter.ImagesGridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.
                from(context).inflate(R.layout.grid_item_layout, parent, false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        rootView.setLayoutParams(lp);
        ImagesRecyclerViewGridAdapter.ImagesGridViewHolder imagesGridViewHolder = new ImagesRecyclerViewGridAdapter.ImagesGridViewHolder(rootView);
        return imagesGridViewHolder;
    }

    @Override
    public void onBindViewHolder(ImagesRecyclerViewGridAdapter.ImagesGridViewHolder holder, int position) {

        holder.titleTv.setText(images.get(position).getTitle());
        Log.d("sm", "title: "+images.get(position).getTitle());
        Picasso.with(context)
                .load(images.get(position).getImage())
                .error(R.drawable.errorstop)
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        if (images != null) {
            return images.size();
        }
        return 0;
    }

    @Override
    public void onViewRecycled(ImagesRecyclerViewGridAdapter.ImagesGridViewHolder holder) {
        super.onViewRecycled(holder);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    public static class ImagesGridViewHolder extends RecyclerView.ViewHolder {
        TextView titleTv;
        ImageView imageView;

        ImagesGridViewHolder(final View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image_grid);
            titleTv = (TextView) itemView.findViewById(R.id.title_grid);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(itemView, position);
                        }
                    }
                }
            });
        }
    }
}